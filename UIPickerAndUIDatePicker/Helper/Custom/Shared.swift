//
//  Shared.swift
//  KosignBooks
//
//  Created by Huort Seanghay on 20/9/22.
//

import Foundation

enum LanguageCode: String {
    case Cambodia   = "km"
    case English    = "en"
}

struct Shared {
    static var share = Shared()
    static var language : LanguageCode  = .Cambodia

    func getLocalizedFont(preFontName: String) -> String {
        
        var fontName = "notoserifkhmer-semicondensed";
        
        if Shared.language.rawValue == LanguageCode.English.rawValue {
            fontName = "Rubik";
        }
        
        if Shared.language.rawValue == LanguageCode.English.rawValue { //English
            
            if preFontName.range(of: "notoserifkhmer-semicondensed") != nil {//Khmer
                if preFontName.range(of: "bold") != nil {
                    fontName += "-bold"
                }
                else if preFontName.range(of: "medium") != nil {
                    fontName += "-medium"
                } else {
                    fontName += "-regular"
                }
            } else if preFontName.range(of: "nanumsquare") != nil { //Korean
                if preFontName.range(of: "eb") != nil { //Bold
                    fontName += "-bold"
                } else if preFontName.range(of: "b") != nil {
                    fontName += "-medium"
                } else {
                    fontName += "-regular"
                }
            } else if preFontName.range(of: "nanumsquare") != nil { //vietnamese
                if preFontName.range(of: "eb") != nil { //Bold
                    fontName += "-bold"
                } else if preFontName.range(of: "b") != nil {
                    fontName += "-medium"
                } else {
                    fontName += "-regular"
                }
            }else {
                fontName = preFontName
            }
            
        } else if Shared.language == .Cambodia { //Khmer
            
            if preFontName.range(of: "inter") != nil {//English
                if preFontName.range(of: "bold") != nil {
                    fontName += "bold"
                } else if preFontName.range(of: "medium") != nil {
                    fontName += "medium"
                } else if preFontName.range(of: "black") != nil {
                    fontName = "inter-black"
                } else {
                    fontName += ""
                }
            }
            else if preFontName.range(of: "nanumsquare") != nil {//Korean
                if preFontName.range(of: "eb") != nil { //Bold
                    fontName += "bold"
                } else if preFontName.range(of: "b") != nil {
                    fontName += "medium"
                } else {
                    fontName += ""
                }
            } else if preFontName.range(of: "nanumsquare") != nil {//vietnamese
                if preFontName.range(of: "eb") != nil { //Bold
                    fontName += "bold"
                } else if preFontName.range(of: "b") != nil {
                    fontName += "medium"
                } else {
                    fontName += ""
                }
            }else {
                fontName = preFontName
            }
            
        }
        
        return fontName
    }

}
