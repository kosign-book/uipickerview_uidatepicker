//
//  TextfieldCell.swift
//  KosignBooks
//
//  Created by Huort Seanghay on 16/9/22.
//

import UIKit

class TextfieldCell: UITableViewCell {
    
    //@IBOutlet
    @IBOutlet weak var titleLbl         : UILabel!
    @IBOutlet weak var textfieldView    : UIView!
    @IBOutlet weak var valueTxt         : UITextField!
    @IBOutlet weak var pickBtn          : UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    // When start tpye in textField show border style
    @IBAction func textFieldDidBegin(_ sender: Any) {
        self.textfieldView.addBorder()
    }
    
    // When end tpye in textField show border style
    @IBAction func textFieldDidEnd(_ sender: Any) {
        self.textfieldView.endChangeBorder()
    }
    
    // For config data
    func ConfigData(data: SignUpModel){
        self.titleLbl.text          = data.title
        self.valueTxt.text          = data.value
        self.valueTxt.placeholder   = data.placeHolder
    }
}
