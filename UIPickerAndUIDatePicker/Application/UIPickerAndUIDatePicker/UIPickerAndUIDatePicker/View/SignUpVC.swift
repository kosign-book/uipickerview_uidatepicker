//
//  SignUpVC.swift
//  KosignBooks
//
//  Created by Huort Seanghay on 14/9/22.
//
 
import UIKit

class SignUpVC: UIViewController {

    //Connect Outlet View
    @IBOutlet weak var picker                       : UIPickerView!
    //Button
    @IBOutlet weak var donePickerButton             : UIButton!
    @IBOutlet weak var continueButton               : UIButton!
    //Picker and DataPicker constraint
    @IBOutlet weak var bottomPickerViewConstraint   : NSLayoutConstraint!
    
    @IBOutlet weak var tableView    : UITableView!
    
    //Connect Outlet View
    @IBOutlet weak var datePicker                       : UIDatePicker!
    //Button
    @IBOutlet weak var doneDatePickerButton             : UIButton!
    //Picker and DataPicker constraint
    @IBOutlet weak var bottomDatePickerViewConstraint   : NSLayoutConstraint!
    
    let signUpVm = SignUpVM()
    
    var pickerData = [
        "Cambodia",
        "China",
        "India",
        "Greek",
        "Roman"
    ]

    override func viewDidLoad() {
        super.viewDidLoad()
       
        // call setSignUpData() from signUpVm
        self.signUpVm.setSignUpData()
        
        //set title to button
        self.continueButton.setTitle("Continue", for: .normal)
       
        //register tableViewCell
        tableView.register(UINib(nibName: "TextfieldCell", bundle: nil), forCellReuseIdentifier: "TextfieldCell")
        
        //Looks for single or multiple taps.
//        let tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
//        view.addGestureRecognizer(tap)
    }
    
    //MARK: - Func
    
    // Format date to 09/12/2018
    func formatDate(date: Date) -> String
    {
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        return formatter.string(from: date)
    }
    
    //MARK: - Private Func
    //For show DatePicker
    private func showDatePicker() {
        UIView.animate(withDuration: 0.3) {
            self.bottomDatePickerViewConstraint.constant = 0
            self.view.layoutIfNeeded()
        }
        self.hidePicker()
    }
    
    //For show picker
    private func showPicker() {
        UIView.animate(withDuration: 0.3) {
            self.bottomPickerViewConstraint.constant = 0
            self.view.layoutIfNeeded()
        }
        self.hideDatePicker()
    }
    
    // For hide DatePicker
    private func hideDatePicker() {
        UIView.animate(withDuration: 0.3) {
            self.bottomDatePickerViewConstraint.constant = -240
            self.view.layoutIfNeeded()
        }
    }
    
    //For hide picker
    private func hidePicker() {
        UIView.animate(withDuration: 0.3) {
            self.bottomPickerViewConstraint.constant = -240
            self.view.layoutIfNeeded()
        }
    }
    
    //Calls this function when the tap is recognized.
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
}

//MARK: - UIPickerViewDelegate,UIPickerViewDataSource -
extension SignUpVC: UIPickerViewDelegate, UIPickerViewDataSource {
        
    // Asks the data source for the number of components in the picker view.
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // Asks the data source for the number of rows for a specified component.
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    // Called by the picker view when it needs the row width to use for drawing row content.
    func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        250
    }
    
    // Called by the picker view when it needs the row height to use for drawing row content.
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        40
    }
    
    // Called by the picker view when it needs the styled title to use for a given row in a given component.
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int)
        -> NSAttributedString? {
        // custom title
        let data = Array(pickerData)[row]
        let myTitle = NSAttributedString(string     : data,
                                         attributes : [NSAttributedString.Key.foregroundColor: UIColor.link,
                                                       NSAttributedString.Key.backgroundColor: UIColor.lightGray])
        return myTitle
    }
    
    // Called by the picker view when it needs the view to use for a given row in a given component.
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
       
        //Custom text label
        var pickerLabel : UILabel
        if let label = view as? UILabel {
            pickerLabel = label
        } else {
            pickerLabel = UILabel()
            pickerLabel.textColor = UIColor.black
            pickerLabel.textAlignment = NSTextAlignment.center
        }
        pickerLabel.text            = Array(pickerData)[row]
        pickerLabel.font            = UIFont(name: "Rubik-Medium", size: 20)
        pickerLabel.sizeToFit()
        return pickerLabel
    }
    
    // Called by the picker view when the user selects a row in a component.
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.picker.selectedRow(inComponent: 0)
    }
}


extension SignUpVC: UITableViewDelegate, UITableViewDataSource {
    
    //Tells the data source to return the number of rows in a given section of a table view.
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return signUpVm.cells.count
    }

    //Asks the data source for a cell to insert in a particular location of the table view.
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let row = signUpVm.cells[indexPath.row]
        
        switch row.type{
        
        // Case for DatePickerView
        case .PickDOB:
            let cell = tableView.dequeueReusableCell(withIdentifier: "TextfieldCell", for: indexPath) as! TextfieldCell
            cell.ConfigData(data: row as! SignUpModel)
            
            //when click on pickBtn then show pop up DatePickerView
            cell.pickBtn.didTap {
                let tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
                self.view.addGestureRecognizer(tap)
                self.datePicker.datePickerMode           = .date
                self.datePicker.preferredDatePickerStyle = .wheels
               
                //when click on doneDatePickerButton
                self.doneDatePickerButton.didTap {
                    // format date from datePicker then throw into textfield
                    cell.valueTxt.text  = self.formatDate(date: self.datePicker.date)
                    cell.valueTxt.resignFirstResponder()
                    // to hide DatePicker pop up
                    self.hideDatePicker()
                }
                //to show DatePicker pop up
                self.showDatePicker()
            }
            return cell
            
        // Case for PickerView
        case .PickRegion:
            let cell = tableView.dequeueReusableCell(withIdentifier: "TextfieldCell", for: indexPath) as! TextfieldCell
            cell.valueTxt.tag       = indexPath.row
            //when click on pickBtn then show pop up PickerView
            cell.pickBtn.didTap {
                self.showPicker()
                self.donePickerButton.didTap {
                    // throw value from pickerData into textfield
                    cell.valueTxt.text  =  self.pickerData[indexPath.row]
                    cell.valueTxt.resignFirstResponder()
                    self.hidePicker()
                }
            }
            cell.ConfigData(data: row as! SignUpModel)
            return cell
            
        default:
            return UITableViewCell()
        }
    }
}
